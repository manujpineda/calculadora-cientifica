/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author Juan manuel Pineda
 */
public class Calculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Operacionar miOperacion1;
        miOperacion1 = new Operacionar();
        Operaciontrigo miOperaciontrigo;
        miOperaciontrigo = new Operaciontrigo();
        
        miOperacion1.N1=40;
        miOperacion1.N2=2;
        System.out.println("El valor para las funciones trigonometricas debe ser expresado en radianes");
        miOperaciontrigo.N1=3.1415192;
        
        System.out.println("La suma es:");
        System.out.println(miOperacion1.suma());
        System.out.println("La resta  es:");
        System.out.println(miOperacion1.resta());
        System.out.println("La multiplicación es:");
        System.out.println(miOperacion1.multiplicacion());
        System.out.println("La división es:");
        System.out.println(miOperacion1.division());
        
        System.out.println("El valor del seno es:");
        System.out.println(miOperaciontrigo.seno());
        System.out.println("El valor del coseno es:");
        System.out.println(miOperaciontrigo.coseno());
        System.out.println("El valor de la tangente es:");
        System.out.println(miOperaciontrigo.tangente());
        System.out.println("El valor de la cotangente es:");
        System.out.println(miOperaciontrigo.cotangente());
        System.out.println("El valor de la secante es:");
        System.out.println(miOperaciontrigo.secante());
        System.out.println("El valor de la cosecante es:");
        System.out.println(miOperaciontrigo.cosecante());
        
                
           
    }
    
}
